import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	kotlin("jvm") version "1.6.10"
	kotlin("kapt") version "1.6.10"
	id("com.github.johnrengelman.shadow") version "7.0.0"
}

group = "com.butilov"
version = "1.0"

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.jetbrains.kotlin:kotlin-reflect:1.6.10")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.6.10")

	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.0")
	implementation("io.projectreactor:reactor-core:3.4.14")
	implementation("com.typesafe.akka:akka-actor-typed_2.13:2.6.18")

	implementation("org.openjdk.jmh:jmh-core:1.34")
	kapt("org.openjdk.jmh:jmh-generator-annprocess:1.34")

	testImplementation("org.junit.jupiter:junit-jupiter:5.8.2")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "17"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
