package com.butilov.async

import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.publisher.toFlux
import reactor.core.scheduler.Schedulers
import java.time.Duration

fun runSimpleBlocking() {
    Flux.range(1, 100_000)
        .flatMap { sleepBlockingAndPrint() }
        .subscribeOn(Schedulers.parallel())
        .blockLast()
}

fun runSimpleNonBlocking() {
    Flux.range(1, 100_000)
        .flatMap { sleepNonblockingAndPrint() }
        .subscribeOn(Schedulers.parallel())
        .blockLast()
}

fun runParallelNonBlocking() {
    Flux.range(1, 100_000)
        .parallel()
        .runOn(Schedulers.parallel())
        .flatMap { Mono.delay(Duration.ofMillis(1000)) }
        .doOnNext { print(".") }
        .toFlux()
        .blockLast()
}

fun runParallelNonBlocking2() {
    Flux.range(1, 100_000)
        .flatMap(
            {
                Mono.defer { Mono.delay(Duration.ofMillis(1000)) }.subscribeOn(Schedulers.boundedElastic())
            },
            100_000 // concurrency
        )
        .doOnNext { print(".") }
        .blockLast()
}

private fun sleepBlockingAndPrint(): Mono<Int> {
    Thread.sleep(1000)
    println(Thread.currentThread().name)
    return Mono.empty()
}

private fun sleepNonblockingAndPrint(): Mono<Long> = Mono.delay(Duration.ofMillis(1000)).also {
    print(".")
}
