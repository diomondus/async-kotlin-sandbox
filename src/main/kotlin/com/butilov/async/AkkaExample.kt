package com.butilov.async

import akka.actor.AbstractActor
import akka.actor.AbstractActorWithTimers
import akka.actor.ActorRef
import java.time.Duration
import java.util.concurrent.CompletableFuture

const val cmd_run = "run"
const val cmd_wait = "wait"
const val cmd_dot = "."
const val cmd_complete = "complete"

class DotActor : AbstractActorWithTimers() {
    private lateinit var supervisorActorRef: ActorRef

    override fun createReceive(): Receive = receiveBuilder()
        .matchEquals(cmd_wait) {
            supervisorActorRef = sender()
            timers.startSingleTimer(cmd_wait, cmd_dot, Duration.ofMillis(1000))
        }
        .matchEquals(cmd_dot) {
            print(".")
            supervisorActorRef.tell(cmd_complete, self)
        }
        .build()
}

class SupervisorActor(
    private val dotActorRef: ActorRef,
    private val resultFuture: CompletableFuture<Boolean>
) : AbstractActor() {

    private val times = 100000
    private var completed = 0

    override fun createReceive(): Receive = receiveBuilder()
        .matchEquals(cmd_run) {
            repeat(times) {
                dotActorRef.tell(cmd_wait, self)
            }
        }
        .matchEquals(cmd_complete) {
            completed++
            if (completed == times) {
                resultFuture.complete(true)
            }
        }
        .build()
}